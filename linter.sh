flake8 . --max-line-length=120
pylint --max-line-length=120 src/ tests/ --disable=missing-function-docstring,missing-module-docstring,missing-class-docstring
