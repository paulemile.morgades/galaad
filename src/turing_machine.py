from dataclasses import dataclass
from typing import Mapping, Tuple, List
from enum import Enum


class Direction(Enum):
    LEFT = -1
    RIGHT = 1


class State(str):
    pass


class Symbol(str):
    pass


TransitionFunction = Mapping[Tuple[State, Symbol], Tuple[State, Symbol, Direction]]


@dataclass
class TuringMachine:
    collections_of_states: List[State]
    alphabet: List[Symbol]
    initial_state: State
    blank_symbol: Symbol
    # This is not a function
    transition_function: TransitionFunction
    collection_of_accepting_states: List[State]
