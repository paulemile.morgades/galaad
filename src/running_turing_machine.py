"""
Run a given TuringMachine.
"""
from typing import List, Tuple

from src.tape import Tape
from src.turing_machine import TuringMachine, Symbol, State


def initialize_tape(collection_of_inputs: List[Symbol], blank_symbol: Symbol):
    """
    Initialize a TuringMachine tape.

    ([A,B,C],D)-> Tape({0:A,1:B,2:C},D)
    """
    map_of_index_and_symbol = dict(enumerate(collection_of_inputs))
    return Tape(map_of_index_and_symbol, blank_symbol)


def run_turing_machine(turing_machine: TuringMachine, input_tape: List[Symbol]) -> Tuple[List[Symbol], bool]:
    """
    Run a given Turing machine his input tape.

    :param turing_machine:
    :param input_tape:
    :return: It returns the output tape and bool stating if the turing machine was in accepting state or not.
    """
    tape: Tape = initialize_tape(input_tape, turing_machine.blank_symbol)
    current_state: State = turing_machine.initial_state
    index: int = 0
    while current_state not in turing_machine.collection_of_accepting_states:
        current_symbol = tape[index]
        if (current_state, current_symbol) not in turing_machine.transition_function:
            break
        current_state, new_symbol, direction = turing_machine.transition_function[current_state, current_symbol]
        tape[index] = new_symbol
        index += direction.value
    machine_is_in_accepting_test = current_state in turing_machine.collection_of_accepting_states
    return tape.get_values(), machine_is_in_accepting_test
