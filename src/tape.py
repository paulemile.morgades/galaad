# pylint: disable=missing-module-docstring
from dataclasses import dataclass
from typing import Dict

from src.turing_machine import Symbol


@dataclass
class Tape:
    """
    Turing machine tape.
    The int of the dict is the index of the value in the tape.
    This is a valid tape: {-1:A,0:B,1:C}
    """
    content: Dict[int, Symbol]
    blank_symbol: Symbol

    def __getitem__(self, item):
        if item not in self.content:
            self.content[item] = self.blank_symbol
            return self.blank_symbol
        return self.content[item]

    def __setitem__(self, key, value):
        self.content[key] = value

    def get_values(self):
        return list(self.content.values())
