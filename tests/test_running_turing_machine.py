from src.running_turing_machine import initialize_tape, run_turing_machine
from src.turing_machine import Symbol, TuringMachine, State, Direction


def test_initialize_tape1():
    # Given
    blank_symbol = Symbol('a')
    # When
    result = initialize_tape([blank_symbol, Symbol('b'), Symbol('c')], blank_symbol)
    # Then
    reference = {0: 'a', 1: 'b', 2: 'c'}
    assert result.content == reference
    assert result.blank_symbol == blank_symbol


def test_initialize_tape2():
    # Given
    blank_symbol = Symbol('blank')
    # When
    result = initialize_tape([Symbol('a'), Symbol('b'), Symbol('c'), Symbol('1234')], blank_symbol)
    # Then
    reference = {0: 'a', 1: 'b', 2: 'c', 3: '1234'}
    assert result.content == reference
    assert result.blank_symbol == blank_symbol


def test_run_turing_machine_one_step():
    # Given
    initial_state = State('a')
    accepting_state = State('b')
    collection_of_states = [initial_state, accepting_state]
    blank_symbol = Symbol('0')
    alphabet = [blank_symbol, Symbol('1')]
    transition_function = {(State('a'), Symbol('0')): (State('b'), Symbol('1'), Direction.LEFT)}
    collection_of_accepting_states = [accepting_state]
    turing_machine = TuringMachine(collection_of_states, alphabet, initial_state, blank_symbol, transition_function,
                                   collection_of_accepting_states)
    input_tape = [Symbol('0')]
    tape, is_accepting = run_turing_machine(turing_machine, input_tape)
    assert tape == ['1']
    assert is_accepting


def test_run_turing_machine_two_step():
    # Given
    initial_state = State('a')
    accepting_state = State('c')
    collection_of_states = [initial_state, State('b'), accepting_state]
    blank_symbol = Symbol('0')
    alphabet = [Symbol('0'), Symbol('1')]
    transition_function = {(State('a'), Symbol('0')): (State('b'), Symbol('1'), Direction.LEFT),
                           (State('b'), Symbol('0')): (State('c'), Symbol('1'), Direction.LEFT)}
    collection_of_accepting_states = [accepting_state]
    turing_machine = TuringMachine(collection_of_states, alphabet, initial_state, blank_symbol, transition_function,
                                   collection_of_accepting_states)
    input_tape = [Symbol('0')]
    tape, is_accepting = run_turing_machine(turing_machine, input_tape)
    assert tape == ['1', '1']
    assert is_accepting

# TODO test when it is not accepting
